$("header").load("header.html").ready(function() {
    $("#casinosNavigationHidden").load("navigation.html #collapseMenu");
});

function switchTheme(el) {
  $('.' + el).toggleClass('light dark');
}

function toggleMenu() {
  $('.nav-container').collapse('toggle');
  $('.nav-overlay').toggleClass('collapse');
}
